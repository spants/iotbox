#ifndef _USER_CONFIG_H_
#define _USER_CONFIG_H_

#define ICACHE_STORE_TYPEDEF_ATTR __attribute__((aligned(4),packed))
#define ICACHE_STORE_ATTR __attribute__((aligned(4)))
#define ICACHE_RAM_ATTR __attribute__((section(".iram0.text")))
#define ICACHE_RODATA_ATTR __attribute__((section(".irom.text")))

#define CFG_HOLDER	0x00F8F6F3
#define CFG_LOCATION	0x3f8

#define FWVER "v4.1"

/*DEFAULT CONFIGURATIONS*/

#define STA_MODE     "dhcp"
#define STA_IP       "192.168.1.17"
#define STA_MASK     "255.255.255.0"
#define STA_GW       "192.168.1.1"
#define STA_SSID     "iotbox"
#define STA_PASS     "robotnet"
#define STA_TYPE AUTH_WPA2_PSK

#define AP_IP        "192.168.4.1"
#define AP_MASK      "255.255.255.0"
#define AP_GW        "192.168.4.1"

#define HTTPD_PORT      80
#define HTTPD_AUTH      0
#define HTTPD_USER      "admin"
#define HTTPD_PASS      "pass"

#define BROADCASTD_ENABLE	0

#define BROADCASTD_HOST		"http://192.168.4.2:80/maxrest/rest/os/mxmeterdata"
#define BROADCASTD_URL		"ASSETNUM=11430&METERNAME=TEMP-C&NEWREADING=%s&_lid=maxadmin&_lpwd=maxadmin&SITEID=BEDFORD"

#define NTP_ENABLE    0
#define NTP_TZ  	  2

#define MQTT_ENABLE			0
#define MQTT_HOST			"192.168.1.22" //host name or IP "192.168.11.1"
#define MQTT_PORT			1883
#define MQTT_KEEPALIVE		120	 /*seconds*/
#define MQTT_DEVID			"iotbox%08x"
#define MQTT_USER			""
#define MQTT_PASS			""
#define MQTT_USE_SSL		0
#define MQTT_LED_SUBS_TOPIC 		"+/out/led/#"
#define MQTT_DISPLAY_SUBS_TOPIC 	"+/out/display/#"
#define MQTT_BUTTON_PUB_TOPIC  		"iotbox%08x/in/button"

// dht22 temp not used currently - send temp on one topic
#define MQTT_DHT22_TEMP_PUB_TOPIC  	"iotbox%08x/in/temperature"
//

#define MQTT_DHT22_HUMI_PUB_TOPIC   "iotbox%08x/in/humidity"
#define MQTT_DS18B20_TEMP_PUB_TOPIC "iotbox%08x/in/temperature"
#define MQTT_LWT 					"iotbox%08x/lwt"

#define SENSOR_DS18B20_ENABLE     1
#define SENSOR_DHT22_ENABLE       0

#define LED_LATCHING_ENABLE       0
#define LED1NAME       "LED1"
#define LED2NAME       "LED2"
#define LED3NAME       "LED3"
	
#define MQTT_BUF_SIZE		255
#define MQTT_RECONNECT_TIMEOUT 	5	/*second*/
#define MQTT_CONNTECT_TIMER 	5 	/**/
#define IOTFQUICK_ENABLE		0
#define IOTFREG_ENABLE			0
#define IOTF_ORG 					"org_id"
#define IOTF_TOKEN 					"auth_token"

#endif

