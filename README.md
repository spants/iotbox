IoTBox code for ESP8266 (esp12-4MB version)	
- currently using the sdk v1.5 library

Please vist http://blog.spants.com for details

Requires https://github.com/raburton/esptool2 from Richard Burton

Pinouts using the ESP12 adapter board

|Pin    |IoTBox use                           |Future          |
|-------|-------------------------------------|----------------|
|tx     |tx on programmer|   |
|rx     |rx on programmer |  |
|gpio4  |to I2C - SCL screen (GPIO4 is sometimes mislabelled labelled as GPIO5 on breakout board!!!)| |   
|gpio5  |n/c    |Vibration/PIR|
|0v     |gnd | | 
|gpio15 |resistor 150 ohm to led =  RED | | 
|gpio2  |data pin for 1-wire ds18b20 (or dht22 with 10k resistor to vcc) | |  
|gpio0  |switch (optional:held high via 10k resistor?). Other side of switch is at GND | |  
|gpio14 |to I2C - SDA screen  || 
|gpio12 |resistor 150 ohm to led = GREEN| |   
|gpio13 |resistor 150 ohm to led = BLUE | |  
|vcc    |3v3 | |  
|gpio16 |n/c    |Piezo buzzer|
|ch_pd  |n/c (held high on the breakout board)| |   
|adc    |n/c    |Potentiometer / Light detection|
|reset  |n/c| |   
