<html>
<head><title>ESP8266 IoTBox</title>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div id="main">
<h1>IoTBox Menu</h1>
<p>
<ul>
<li><a href="config/wifi/wifi.tpl">WiFi</a> settings.</li>
<li><a href="config/mqtt.tpl">MQTT</a> settings.</li>
<li><a href="config/httpd.tpl">Password</a> settings.</li>
<li><a href="config/sensor.tpl">Sensor</a> settings.</li>
<li><a href="config/broadcastd.tpl">REST</a> settings.</li> 
<li><a href="control/led.html">LED</a> control page.</li>
<br/>
<li>Sensor readings: <a href="control/ds18b20.tpl">Temperature (Default)</a> or <a href="control/dht22.tpl">Temperature/Humidity</a>.</li>
<br/>
<li><a href="control/switch.cgi" onclick="return confirm('Are you sure you want to switch rom?')">Switch rom</a></li>
<li><a href="control/ota.cgi" onclick="return confirm('Are you sure you want to update?')">OTA Update</a></li>
<br/>
<li><a href="control/reset.cgi" onclick="return confirm('Are you sure you want to restart?')">Restart</a> the system.</li>
<br/>
<li><a href="about.tpl">About</a></li>
</ul>
</p>
</div>
</body></html>
