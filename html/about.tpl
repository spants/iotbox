<html>
<head><title>IoTBox</title>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div id="main">
<h1>About</h1>
<p>
IoTBox by Tony.Neal@uk.ibm.com<br/>
<br/>
Based on the Three channel WiFi relay/thermostat software <br/>
by <a href="http://harizanov.com">Martin Harizanov</a>
and others.<br/>
<br/>
OTA updating and rBoot written by <br/>
<a href="https://github.com/raburton">Richard A Burton</a>
<br/><br/>
For more information: <a href="http://blog.spants.com">Tony Neal's Blog</a>
<br/>
</p>
<p>Firmware version: %fwver%</p>
<p>%freeheap%</p>
<br/>

    <button onclick="location.href = '/';" class="float-left submit-button" >Back</button>
    
</div>
</body></html>
