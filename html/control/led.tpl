<html><head><title>LED control</title>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<div id="main">
<h1>LED control page</h1>
<p>
%led1name% is now %led1%. You can change that using the buttons below.
</p>
<form method="get" action="led.cgi">
<input type="submit" name="led1" value="1">
<input type="submit" name="led1" value="0">
</form>
<p>
%led2name% is now %led2%. You can change that using the buttons below.
</p>
<form method="get" action="led.cgi">
<input type="submit" name="led2" value="1">
<input type="submit" name="led2" value="0">
</form>
<p>
%led3name% is now %led3%. You can change that using the buttons below.
</p>
<form method="get" action="led.cgi">
<input type="submit" name="led3" value="1">
<input type="submit" name="led3" value="0">
</form>

<button onclick="location.href = '/';" class="float-left submit-button" >Back</button>
</div>
</body></html>
