var visibleFlag = 1;
var statusMsg = false;
var connected = false;
var doingsave=false;
	
var state = {
    led1: 0,
    led1name: "LED 1",
    led2: 0,
    led2name: "LED 2",
    led3: 0,
    led3name: "LED 3",

};


function update() {

	$("#led1name").html(state.led1name);
	$("#led2name").html(state.led2name);
	$("#led3name").html(state.led3name);

	if (state.led1 == 1) {
		$("#led1").html("ON");
		$("#led1").css("background-color", "#ff9600");
	} else {
		$("#led1").html("OFF");
		$("#led1").css("background-color", "#555");
	}

	if (state.led2 == 1) {
		$("#led2").html("ON");
		$("#led2").css("background-color", "#ff9600");
	} else {
		$("#led2").html("OFF");
		$("#led2").css("background-color", "#555");
	}

	if (state.led3 == 1) {
		$("#led3").html("ON");
		$("#led3").css("background-color", "#ff9600");
	} else {
		$("#led3").html("OFF");
		$("#led3").css("background-color", "#555");
	}


}

$("#led1").click(function () {
	state.led1++;
	if (state.led1 > 1) state.led1 = 0;

    if (state.led1==1) {
        $(this).html("ON");
        $(this).css("background-color", "#ff9600");
    }
    else {
        $(this).html("OFF");
        $(this).css("background-color", "#555");
    }

    save("led1", state.led1);
});

$("#led2").click(function () {
	state.led2++;
	if (state.led2 > 1) state.led2 = 0;

    if (state.led2==1) {
        $(this).html("ON");
        $(this).css("background-color", "#ff9600");
    }
    else {
        $(this).html("OFF");
        $(this).css("background-color", "#555");
    }

    save("led2", state.led2);
});

$("#led3").click(function () {
	state.led3++;
	if (state.led3 > 1) state.led3 = 0;

    if (state.led3==1) {
        $(this).html("ON");
        $(this).css("background-color", "#ff9600");
    }
    else {
        $(this).html("OFF");
        $(this).css("background-color", "#555");
    }

    save("led3", state.led3);
});


// function for checking if the page is visible or not
// (if not visible it will stop updating data)
function checkVisibility() {
    $(window).bind("focus", function(event) {
        visibleFlag = 1;
    });

    $(window).bind("blur", function(event) {
        visibleFlag = 0;
    });
}

function setStatus(msg,dur,pri){	 // show msg on status bar
		if(statusMsg == true){return};
		statusMsg= true;
		if(pri>0){
			$("#statusView").toggleClass("statusViewAlert",true);
			$("#statusView").toggleClass("statusView",false);
		} else {
			$("#statusView").toggleClass("statusView",true);
			$("#statusView").toggleClass("statusViewAlert",false);
		}
		$("#statusView").show();
		$("#statusView").html(msg);
		dur = dur*1000;
		if(dur >0){
			setTimeout(function(){$("#statusView").hide(200);$("#statusView").html(""); statusMsg= false},dur)
		}
	}
	
function save(param, payload) {
	doingsave=true;
    $.ajax({
        type: 'GET',
        url: "led.cgi?" + param + "=" + payload,
        async: true,
		timeout: 3000,
		tryCount : 0,
		retryLimit : 3,
		success: function (data) {
			statusMsg = false;
			if(!connected) setStatus("Connected",2,0); 
			connected=true;
			doingsave=false;
		},
		error : function(xhr, textStatus, errorThrown ) {
        if (textStatus == 'timeout') {
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
                //try again
                $.ajax(this);
                return;
            }            
            return;
        }
		if(connected) setStatus("No connection to server!",0,1);
		connected=false;
		doingsave=false;
    }
    });
}

function server_get() {
    var output = {};
	checkVisibility();
	if (visibleFlag) {
		$.ajax({
			url: "led.cgi",
			dataType: 'json',
			async: true,
			timeout: 3000,
			tryCount : 0,
			retryLimit : 3,
			success: function (data) {
				if (data.length !== 0) {
					statusMsg = false;
					if(!connected) setStatus("Connected",2,0); 
					connected=true;
					if(!doingsave) {
						state = data;
						update();
					}
				}
			},
		error : function(xhr, textStatus, errorThrown ) {
        if (textStatus == 'timeout') {
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
                //try again
                $.ajax(this);
                return;
            }            
            return;
        }
		if(connected) setStatus("No connection to server!",0,1);
		connected=false;
		}
		});
	}
    return;
}

$(document).ready(function() {
	server_get();
	update();
	setInterval(server_get, 5000);
    checkVisibility();
});
