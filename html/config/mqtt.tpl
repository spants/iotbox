<html><head><title>MQTT settings</title>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" type="text/css" href="../style.css">
<script type="text/javascript">

window.onload=function(e) {
	
	sc('mqtt-enable',%mqtt-enable%);
	sc('iotfquick-enable',%iotfquick-enable%);
	sc('mqtt-use-ssl',%mqtt-use-ssl%);
	sc('iotfreg-enable',%iotfreg-enable%);
	di();
};

function sc(l,v) {
document.getElementById(l).checked=v;}

function sd(l,v) {
document.getElementById(l).disabled=v;}

function di(){
var v=true;
sd('iotf-org',v);
sd('iotf-token',v);

if (document.getElementById('mqtt-enable').checked) {
	v=false;
	}

sd('mqtt-host',v);
sd('mqtt-port',v);
sd('mqtt-keepalive',v);
sd('mqtt-user',v);
sd('mqtt-pass',v);
sd('mqtt-use-ssl',v);
sd('mqtt-led-subs-topic',v);
sd('mqtt-display-subs-topic',v);
sd('mqtt-button-pub-topic',v);
sd('mqtt-ds18b20-temp-pub-topic',v);
sd('mqtt-dht22-humi-pub-topic',v);
sd('mqtt-lwt',v);

if (document.getElementById('iotfreg-enable').checked) {
	sd('iotf-org',false);
	sd('iotf-token',false);
	}
}

function mq(){
	//sc('mqtt-enable',true);
	sc('iotfreg-enable',false);
	sc('iotfquick-enable',false);
	di();


}

function i1(){
	sc('mqtt-enable',false);
	sc('iotfreg-enable',false);
	//sc('iotfquick-enable',true);
	di();

}

function i2(){
	sc('mqtt-enable',false);
	sc('iotfquick-enable',false);
	//sc('iotfreg-enable',true);
	di();
}

</script>

</head>
<body>
<div id="main">
<p>
<b>MQTT Settings</b>
</p>
<form name="mqttform" action="mqtt.cgi" method="post">

<table>
<tr><td>Device ID:</td><td>%mqtt-devid%</td></tr>
<tr><td>&nbsp;<td><td>&nbsp;<td></tr>
<tr><td>IBM IoTF Quickstart?:</td><td><input type="checkbox" name="iotfquick-enable" id="iotfquick-enable" onclick="i1();"/>
<script>
document.write('<a href="https://quickstart.internetofthings.ibmcloud.com/">IBM Quickstart Page</a>');
</script>
</td></tr>
<tr><td>&nbsp;<td><td>&nbsp;<td></tr>
<tr><td>IBM IoTF Registered?:</td><td><input type="checkbox" name="iotfreg-enable" id="iotfreg-enable" onclick="i2();"/>
<a href="https://console.ng.bluemix.net/" target="_blank">IBM Bluemix Platform</a></td></tr>
<tr><td>Organisation ID:</td><td><input type="text" name="iotf-org" id="iotf-org" value="%iotf-org%"/>     </td></tr>
<tr><td>Auth_Token:</td><td><input type="text" name="iotf-token" id="iotf-token" value="%iotf-token%"/>     </td></tr>
<tr><td>(Currently Publish Only)</td><td>
<tr><td>&nbsp;<td><td>&nbsp;<td></tr>
<tr><td>MQTT enabled?:</td><td><input type="checkbox" name="mqtt-enable" id="mqtt-enable" onclick="mq();"/></td></tr>
<tr><td>Host:</td><td><input type="text" name="mqtt-host" id="mqtt-host" value="%mqtt-host%"/>     </td></tr>
<tr><td>Port:</td><td><input type="text" name="mqtt-port" id="mqtt-port" value="%mqtt-port%"/>     </td></tr>
<tr><td>Keepalive (sec):</td><td><input type="text" name="mqtt-keepalive" id="mqtt-keepalive" value="%mqtt-keepalive%"/> </td></tr>
<tr><td>User:</td><td><input type="text" name="mqtt-user" id="mqtt-user" value="%mqtt-user%"/>     </td></tr>
<tr><td>Password:</td><td><input type="password" name="mqtt-pass" id="mqtt-pass" value="%mqtt-pass%"/>     </td></tr>
<tr><td>Use SSL?:</td><td><input type="checkbox" name="mqtt-use-ssl" id="mqtt-use-ssl" /> (max 1024 bit key size)  </td></tr>
<tr><td>&nbsp;<td><td>&nbsp;<td></tr>
<tr><td>LED subs topic:</td><td><input type="text" name="mqtt-led-subs-topic" id="mqtt-led-subs-topic" value="%mqtt-led-subs-topic%" size="35">  Must end in /led/#</td></tr>
<tr><td>Display subs topic:</td><td><input type="text" name="mqtt-display-subs-topic" id="mqtt-display-subs-topic" value="%mqtt-display-subs-topic%" size="35">  Must end in /display/#   </td></tr>
<tr><td>Button pub topic:</td><td><input type="text" name="mqtt-button-pub-topic" id="mqtt-button-pub-topic" value="%mqtt-button-pub-topic%" size="35">     </td></tr>
<tr><td>Temperature pub topic:</td><td><input type="text" name="mqtt-ds18b20-temp-pub-topic" id="mqtt-ds18b20-temp-pub-topic" value="%mqtt-ds18b20-temp-pub-topic%" size="35">     </td></tr>
<tr><td>Humidity pub topic:</td><td><input type="text" name="mqtt-dht22-humi-pub-topic" id="mqtt-dht22-humi-pub-topic" value="%mqtt-dht22-humi-pub-topic%" size="35">   DHT22 enabled devices only</td></tr>
<tr><td>Last Will & Testament:</td><td><input type="text" name="mqtt-lwt" id="mqtt-lwt" value="%mqtt-lwt%" size="35">     </td></tr>
<tr><td>&nbsp;<td><td>&nbsp;<td></tr>
<tr><td><button type="button" onClick="parent.location='/'">Back</button><input type="submit" name="save" value="Save and Reboot!"></td></tr>
</table>
</form>

</body>
</html>
