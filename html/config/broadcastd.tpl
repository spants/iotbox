<html><head><title>Broadcaset Daemon Settings</title>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" type="text/css" href="../style.css">
<script type="text/javascript">

window.onload=function(e) {
	sc('broadcastd-enable',%broadcastd-enable%);
	di();
};

function sc(l,v) {
document.getElementById(l).checked=v;}

function sd(l,v) {
if(document.getElementById(l)) document.getElementById(l).disabled=v;}

function di(){
var v=true;
if (document.getElementById('broadcastd-enable').checked) v=false;
sd('broadcastd-host',v);
sd('broadcastd-URL',v);

}

</script>

</head>
<body>
<div id="main">
<p>
<b>REST Broadcast Settings</b>
</p>
<form name="broadcasetdform" action="broadcastd.cgi" method="post">

<table>
<tr><td>REST broadcast enabled?:</td><td><input type="checkbox" name="broadcastd-enable" id="broadcastd-enable" onclick="di();"/></td></tr>
</td></tr><tr><td>REST Address:</td><td><input type="text" name="broadcastd-host" id="broadcastd-host" value="%broadcastd-host%" size="100"/>     </td></tr>
<tr><td>REST Parameters:</td><td><input type="text" name="broadcastd-URL" id="broadcastd-URL" value="%broadcastd-URL%" size="100"/>     </td></tr>
<tr><td><button type="button" onClick="parent.location='/'">Back</button><input type="submit" name="save" value="Save and Reboot!"></td></tr>
</table>
</form>

</body>
</html>