#############################################################
#
# Root Level Makefile
#
# (c) by CHERTS <sleuthhound@gmail.com>
#
#############################################################

BUILD_BASE	= build
FW_BASE		= firmware

FIND ?= find
# if using a relatve path for MKESPFSIMAGE must be relative to html directory
MKESPFSIMAGE ?= ../mkespfsimage/mkespfsimage
FLASH_OPTS ?= -fs 32m

# Base directory for the compiler
XTENSA_TOOLS_ROOT ?= /Volumes/esp8266/esp-open-sdk/xtensa-lx106-elf/bin

# base directory of the ESP8266 SDK package, absolute
SDK_BASE	?= /Volumes/esp8266/esp-open-sdk/sdk
SDK_TOOLS	?= ../../esp-open-sdk
export SDK_BASE

ESPTOOL2 ?= /Volumes/esp8266/code/esptool2/esptool2
FW_SECTS = .text .data .rodata
FW_USER_ARGS = -quiet -bin -boot2 -4096
export ESPTOOL2

# esptool path and port
#ESPTOOL ?=esptool.py
ESPTOOL ?=$(SDK_TOOLS)/esptool/esptool.py
ESPPORT ?=/dev/cu.SLAB_USBtoUART
ESPBAUD ?=256000


# name for the target project
TARGET = iotbox
RBOOT = $(FW_BASE)/rboot.bin

# which modules (subdirectories) of the project to include in compiling
MODULES		= user driver rboot/appcode
EXTRA_INCDIR	= include \
		. \
		lib/heatshrink/ \
		rboot

# libraries used in this project, mainly provided by the SDK
LIBS = c gcc hal phy pp net80211 lwip wpa main2 crypto

# compiler flags using during compilation of source files
CFLAGS = -Os -g -O2 -std=c99 -Wno-implicit -Wpointer-arith -Wundef -Werror -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals  -D__ets__ -DICACHE_FLASH -DLWIP_OPEN_SRC -DBOOT_BIG_FLASH

# linker flags used to generate the main object file
LDFLAGS = -nostdlib -Wl,--no-check-sections -u call_user_start -u Cache_Read_Enable_New -Wl,-static

# linker script used for the above linker step
LD_SCRIPT = eagle.app.v6.ld

# various paths from the SDK used in this project
SDK_LIBDIR	= lib
SDK_LDDIR	= ld
SDK_INCDIR	= include include/json

# select which tools to use as compiler, librarian and linker
CC		:= $(XTENSA_TOOLS_ROOT)/xtensa-lx106-elf-gcc
AR		:= $(XTENSA_TOOLS_ROOT)/xtensa-lx106-elf-ar
LD		:= $(XTENSA_TOOLS_ROOT)/xtensa-lx106-elf-gcc
OBJCOPY := $(XTENSA_TOOLS_ROOT)/xtensa-lx106-elf-objcopy
OBJDUMP := $(XTENSA_TOOLS_ROOT)/xtensa-lx106-elf-objdump

# no user configurable options below here
SRC_DIR		:= $(MODULES)
BUILD_DIR	:= $(addprefix $(BUILD_BASE)/,$(MODULES))

SDK_LIBDIR	:= $(addprefix $(SDK_BASE)/,$(SDK_LIBDIR))
SDK_INCDIR	:= $(addprefix -I$(SDK_BASE)/,$(SDK_INCDIR))

# targets for making libmain2.a
LIBMAIN_SRC = $(addprefix $(SDK_LIBDIR)/,libmain.a)
LIBMAIN_DST = $(addprefix $(BUILD_BASE)/,libmain2.a)

SRC		:= $(foreach sdir,$(SRC_DIR),$(wildcard $(sdir)/*.c))
OBJ		:= $(patsubst %.c,$(BUILD_BASE)/%.o,$(SRC))
LIBS		:= $(addprefix -l,$(LIBS))
APP_AR		:= $(addprefix $(BUILD_BASE)/,$(TARGET).a)
TARGET_OUT	:= $(addprefix $(BUILD_BASE)/,$(TARGET).out)

LD_SCRIPT	:= $(addprefix -T,$(LD_SCRIPT))

INCDIR	:= $(addprefix -I,$(SRC_DIR))
EXTRA_INCDIR	:= $(addprefix -I,$(EXTRA_INCDIR))
MODULE_INCDIR	:= $(addsuffix /include,$(INCDIR))

vpath %.c $(SRC_DIR)

define compile-objects
$1/%.o: %.c
	@echo "CC $$<"
	@$(CC) $(INCDIR) $(MODULE_INCDIR) $(EXTRA_INCDIR) $(SDK_INCDIR) $(CFLAGS)  -c $$< -o $$@
endef

.PHONY: all clean flash

all: $(BUILD_DIR) $(FW_BASE) $(RBOOT) $(LIBMAIN_DST) $(FW_BASE)/$(TARGET).bin $(FW_BASE)/$(TARGET).espfs.bin

$(LIBMAIN_DST): $(LIBMAIN_SRC)
	@echo "OC $@"
	@$(OBJCOPY) -W Cache_Read_Enable_New $^ $@

$(RBOOT):
	@$(MAKE) XTENSA_BINDIR=$(XTENSA_TOOLS_ROOT) RBOOT_BUILD_BASE=../$(BUILD_BASE)/rboot RBOOT_FW_BASE=../$(FW_BASE) RBOOT_BIG_FLASH=1 -C rboot

$(TARGET_OUT): $(APP_AR)
	@echo "LD $@"
	@$(LD) -L$(SDK_LIBDIR) -L$(BUILD_BASE) $(LD_SCRIPT) $(LDFLAGS) -Wl,--start-group $(APP_AR) $(LIBS) -Wl,--end-group -o $@
	@echo "------------------------------------------------------------------------------"
	@echo "Section info:"
	@$(OBJDUMP) -h -j .data -j .rodata -j .bss -j .text -j .irom0.text $@
	@echo "------------------------------------------------------------------------------"
#	$echo "Section info:"
#	$(SDK_TOOLS)/memanalyzer.exe $(OBJDUMP).exe $@
#	$echo "------------------------------------------------------------------------------"

$(FW_BASE)/$(TARGET).bin: $(TARGET_OUT)
	@echo "FW $@"
	@$(ESPTOOL2) $(FW_USER_ARGS) $(TARGET_OUT) $@ $(FW_SECTS)

$(FW_BASE)/$(TARGET).espfs.bin: $(FW_BASE)
	@echo "FS $@"
	cd html; $(FIND) .| $(MKESPFSIMAGE) > ../$@; cd ..

$(APP_AR): $(OBJ)
	@echo "AR $@"
	@$(AR) cru $@ $^

$(BUILD_DIR):
	@mkdir -p $@

$(FW_BASE):
	@mkdir -p $@




flash: all
#		@echo "Copy, paste and run this line (doesn't seem to work called from here, odd)"
#		@echo $(ESPTOOL) -p $(ESPPORT) -b $(ESPBAUD) write_flash $(FLASH_OPTS) 0x00000 rboot.bin 0x02000 $(FW_BASE)/$(TARGET).bin 0x80000 $(FW_BASE)/$(TARGET).espfs.bin
	$(ESPTOOL) -p $(ESPPORT) -b $(ESPBAUD) write_flash $(FLASH_OPTS) 0x00000 $(RBOOT) 0x02000 $(FW_BASE)/$(TARGET).bin 0x80000 $(FW_BASE)/$(TARGET).espfs.bin
	


rebuild: clean all

clean:
	@echo "RM $(BUILD_BASE)"
	@rm -rf $(BUILD_BASE)
	@echo "RM $(FW_BASE)"
	@rm -rf $(FW_BASE)

flashinit:
	@echo "Flash init data:"
#	$(ESPTOOL) -p $(ESPPORT) -b $(ESPBAUD) write_flash $(FLASH_OPTS) 0x3f8000 blank.bin 0x3fc000 blank.bin
	$(ESPTOOL) -p $(ESPPORT) -b $(ESPBAUD) write_flash $(FLASH_OPTS) 0x3fc000 blank.bin 0x3fd000 blank.bin 0x3fe000 blank.bin


$(foreach bdir,$(BUILD_DIR),$(eval $(call compile-objects,$(bdir))))
