#include <c_types.h>
#include <osapi.h>
#include <user_interface.h>
#include <mem.h>

#include "httpdconfig.h"
#include "rboot-ota.h"

// added next 3 lines
#include "driver/i2c.h"
#include "driver/i2c_oled.h"

void StartUpdate(bool flash_espfs);

void ICACHE_FLASH_ATTR OtaSwitch() {
	uint8 before, after;
	before = rboot_get_current_rom();
	if (before == 0) after = 1; else after = 0;
	os_printf("Swapping from rom %d to rom %d.\r\n", before, after);
	rboot_set_current_rom(after);
}

static void ICACHE_FLASH_ATTR OtaUpdate_CallBack(bool result, uint8 rom_slot) {

	if(result == true) {
		if (rom_slot != FLASH_BY_ADDR) {
			os_printf("Rom flashed, updating espfs...\r\n");
			rboot_ota_start((ota_callback)OtaUpdate_CallBack, true);
		} else {
			uint8 slot = rboot_get_current_rom();
			if (slot == 0) slot = 1;
			else slot = 0;
			os_printf("Firmware updated, rebooting to rom %d...\r\n", slot);
			OLED_Print(0, 2, (unsigned char *)" ** Complete **", 2);
			rboot_set_current_rom(slot);
			system_restart();
		}
	} else {
		OLED_Print(0, 2, (unsigned char *)"  ** FAILED ** ", 2);
		os_printf("Firmware update failed!\r\n");
	}
}

#define HTTP_HEADER "Connection: keep-alive\r\n\
Cache-Control: no-cache\r\n\
User-Agent: rBoot-Sample/1.0\r\n\
Accept: */*\r\n\r\n"


void ICACHE_FLASH_ATTR OtaUpdate() {

	OLED_CLS();
	unsigned char displaystr[32];
	OLED_Print(7, 0, (unsigned char *)"IoTBox", 1);
	OLED_Print(0, 7, (unsigned char *)"Tony.Neal@uk.ibm.com", 1);
	//OLED_Print(0, 2, (unsigned char *)" OTA UPDATING", 2);

	// start the upgrade process
	if (rboot_ota_start((ota_callback)OtaUpdate_CallBack, false)) {
		os_printf("Updating...\r\n");
		OLED_Print(0, 2, (unsigned char *)" ** Updating **", 2);
	} else {
		OLED_Print(0, 2, (unsigned char *)"  ** FAILED **", 2);
		os_printf("Updating failed!\r\n\r\n");
	}
}
