
/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * Jeroen Domburg <jeroen@spritesmods.com> wrote this file. As long as you retain 
 * this notice you can do whatever you want with this stuff. If we meet some day, 
 * and you think this stuff is worth it, you can buy me a beer in return. 
 * ----------------------------------------------------------------------------
 */

#include "ets_sys.h"
#include "c_types.h"
#include "user_interface.h"
#include "espconn.h"
#include "mem.h"
#include "osapi.h"
#include "gpio.h"
#include "config.h"

// added by tony
#include "mqtt.h"

#include "espmissingincludes.h"
#define BTNGPIO 0

MQTT_Client mqttClient;

static ETSTimer resetBtntimer;

 char currGPIO12State=0;
 char currGPIO13State=0;
 char currGPIO15State=0;

void ICACHE_FLASH_ATTR ioGPIO(int ena, int gpio) {
	//gpio_output_set is overkill. ToDo: use better macros
	if (ena) {
		gpio_output_set((1<<gpio), 0, (1<<gpio), 0);
	} else {
		gpio_output_set(0, (1<<gpio), (1<<gpio), 0);
	}
}

static void ICACHE_FLASH_ATTR resetBtnTimerCb(void *arg) {
	static int resetCnt=0;
	if (!GPIO_INPUT_GET(BTNGPIO)) {
		resetCnt++;
	} else {
		if (resetCnt>=1) { //0.5 sec pressed
				// set mqtt out
				os_printf("MQTT Button triggered...\n");
				// mqtt stuff here
				if(sysCfg.mqtt_enable==1) {
					os_printf("Sending MQTT Button\n");
					char topic[128];
					os_sprintf(topic,"%s",sysCfg.mqtt_button_pub_topic);
					MQTT_Publish(&mqttClient,topic,"1",1,0,0);
					os_printf("Published \"%s\" to topic \"%s\"\n","1",topic);
			    }
				if(sysCfg.iotfquick_enable==1||sysCfg.iotfreg_enable==1) {
					os_printf("Sending IoTF Button\n");

					char temp1[32];
					int len;

					os_sprintf(temp1,"{\"d\":{\"Button\":1}}");

					len = os_strlen(temp1);
					MQTT_Publish(&mqttClient,"iot-2/evt/status/fmt/json",temp1,len,0,0);
					os_printf("Published %s to IoTF\n",temp1);

			    }
		}

		if (resetCnt>=10) { //5 sec pressed
			wifi_station_disconnect();
			wifi_set_opmode(0x3); //reset to AP+STA mode
			os_printf("Reset data. Restarting system...\n");
			sysCfg.cfg_holder = 0x00FFFFFF;
			CFG_Save();
			system_restart();
		}
		resetCnt=0;
	}
}

void ICACHE_FLASH_ATTR ioInit() {
	
	//Set to output mode - these may change to pwm later
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U, FUNC_GPIO12);
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTCK_U, FUNC_GPIO13);
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDO_U, FUNC_GPIO15);

//	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0); = button

	// gpio 14 and 4 are the oled screen
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO4_U, FUNC_GPIO4);

	//set GPIO to init state
 	GPIO_OUTPUT_SET(0,(1<<0));
  	//GPIO_OUTPUT_SET(2,0);

// turn all leds off
		ioGPIO(0,12);
		ioGPIO(0,13);
		ioGPIO(0,15);


	//gpio_output_set(0, 0, (1<<12), (1<<BTNGPIO));
	//gpio_output_set(0, 0, (1<<13), (1<<14));
	
	os_timer_disarm(&resetBtntimer);
	os_timer_setfn(&resetBtntimer, resetBtnTimerCb, NULL);
	os_timer_arm(&resetBtntimer, 500, 1);
}
