

/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * Jeroen Domburg <jeroen@spritesmods.com> wrote this file. As long as you retain 
 * this notice you can do whatever you want with this stuff. If we meet some day, 
 * and you think this stuff is worth it, you can buy me a beer in return. 
 * ----------------------------------------------------------------------------
 */


#include "espmissingincludes.h"
#include "ets_sys.h"
//#include "lwip/timers.h"
#include "user_interface.h"
#include "httpd.h"
#include "io.h"
#include "httpdespfs.h"
#include "cgi.h"
#include "cgiwifi.h"
#include "stdout.h"
#include "auth.h"
#include "sntp.h"
#include "time_utils.h"
#include "config.h"
#include "dht22.h"
#include "ds18b20.h"
#include "broadcastd.h"
#include "wifi.h"
#include "mqtt.h"
#include "httpclient.h"
#include "global.h"

//#include "pwm.h"
//#include "cgipwm.h"

#include "oled.h"
#include "driver/i2c.h"
#include "driver/i2c_oled.h"

#include "string.h"

// these are declared in user_interface.h, so why is the compiler complaining
extern uint8 system_upgrade_flag_check();

uint16_t freq = 1000;

char oledstr[32];
LOCAL os_timer_t startmqtt;

// for new sdk
//void user_rf_pre_init(){}

MQTT_Client mqttClient;

//Function that tells the authentication system what users/passwords live on the system.
//This is disabled in the default build; if you want to try it, enable the authBasic line in
//the builtInUrls below.
int ICACHE_FLASH_ATTR myPassFn(HttpdConnData *connData, int no, char *user, int userLen, char *pass, int passLen) {
	if (no==0) {
		os_strcpy(user, (char *)sysCfg.httpd_user);
		os_strcpy(pass, (char *)sysCfg.httpd_pass);
		return 1;
//Add more users this way. Check against incrementing no for each user added.
//	} else if (no==1) {
//		os_strcpy(user, "user1");
//		os_strcpy(pass, "something");
//		return 1;
	}
	return 0;
}


/*
This is the main url->function dispatching data struct.
In short, it's a struct with various URLs plus their handlers. The handlers can
be 'standard' CGI functions you wrote, or 'special' CGIs requiring an argument.
They can also be auth-functions. An asterisk will match any url starting with
everything before the asterisks; "*" matches everything. The list will be
handled top-down, so make sure to put more specific rules above the more
general ones. Authorization things (like authBasic) act as a 'barrier' and
should be placed above the URLs they protect.
*/
HttpdBuiltInUrl builtInUrls[]={
	{"/", cgiRedirect, "/index.tpl"},
	{"/index.tpl", cgiEspFsTemplate, tplCounter},
	{"/about.tpl", cgiEspFsTemplate, tplCounter},

	//{"/flash.bin", cgiReadFlash, NULL},

	{"/config/*", authBasic, myPassFn},
	{"/control/*", authBasic, myPassFn},

	{"/control/ui.tpl", cgiEspFsTemplate, tplUI},
	{"/control/led.tpl", cgiEspFsTemplate, tplGPIO},
	{"/control/led.cgi", cgiGPIO, NULL},
    {"/control/dht22.tpl", cgiEspFsTemplate, tplDHT},
    {"/control/dht22.cgi", cgiDHT22, NULL}, 
    {"/control/ds18b20.tpl", cgiEspFsTemplate, tplDS18b20},
    {"/control/ds18b20.cgi", cgiDS18b20, NULL}, 
    {"/control/state.cgi", cgiState, NULL}, 
    {"/control/reset.cgi", cgiReset, NULL}, 
	{"/control/ota.cgi", cgiOTA, NULL}, 
	{"/control/switch.cgi", cgiSwitch, NULL}, 
#ifdef CGIPWM_H
	{"/control/pwm.cgi", cgiPWM, NULL},
#endif
	{"/config/wifi", cgiRedirect, "/config/wifi/wifi.tpl"},
	{"/config/wifi/", cgiRedirect, "/config/wifi/wifi.tpl"},
	{"/config/wifi/wifiscan.cgi", cgiWiFiScan, NULL},
	{"/config/wifi/wifi.tpl", cgiEspFsTemplate, tplWlan},
	{"/config/wifi/connect.cgi", cgiWiFiConnect, NULL},
	{"/config/wifi/setmode.cgi", cgiWiFiSetMode, NULL},
	{"/config/wifi/connstatus.cgi", cgiWiFiConnStatus, NULL},
	{"/config/mqtt.tpl", cgiEspFsTemplate, tplMQTT},
	{"/config/mqtt.cgi", cgiMQTT, NULL},
	{"/config/httpd.tpl", cgiEspFsTemplate, tplHTTPD},
	{"/config/httpd.cgi", cgiHTTPD, NULL},
	{"/config/broadcastd.tpl", cgiEspFsTemplate, tplBroadcastD},
	{"/config/broadcastd.cgi", cgiBroadcastD, NULL},
	{"/config/sensor.tpl", cgiEspFsTemplate, tplSensorSettings},
	{"/config/sensor.cgi", cgiSensorSettings, NULL},

	
	{"*", cgiEspFsHook, NULL}, //Catch-all cgi function for the filesystem
	{NULL, NULL, NULL}
};

void ICACHE_FLASH_ATTR mqttConnectedCb(uint32_t *args)
{

	os_timer_disarm(&startmqtt);

	MQTT_Client* client = (MQTT_Client*)args;
	os_printf("MQTT: Connected\r\n");
	if(sysCfg.mqtt_enable==1) {
		MQTT_Publish(client, (uint8_t *)sysCfg.mqtt_lwt,"online",6,0,1);
		MQTT_Subscribe(client, (char *)sysCfg.mqtt_led_subs_topic,0);
		MQTT_Subscribe(client, (char *)sysCfg.mqtt_display_subs_topic,0);
	}
	if(sysCfg.iotfquick_enable==1) {
		os_printf("MQTT: iotf quickstart mode\r\n");
		//quickstart does not subscribe
	}
	if(sysCfg.iotfreg_enable==1) {
		os_printf("MQTT: iotf registered mode\r\n");
		//MQTT_Publish(client, (uint8_t *)sysCfg.mqtt_lwt,"online",6,0,1);
		//MQTT_Subscribe(client, (char *)sysCfg.mqtt_led_subs_topic,0);
		//MQTT_Subscribe(client, (char *)sysCfg.mqtt_display_subs_topic,0);
		MQTT_Subscribe(client, "iot-2/cmd/set/fmt/json",0);
	}

}

void ICACHE_FLASH_ATTR mqttDisconnectedCb(uint32_t *args)
{
	MQTT_Client* client = (MQTT_Client*)args;
	os_printf("MQTT: Disconnected\r\n");
	unsigned char displaystr[32];
	os_sprintf((char *)displaystr, "* MQTT Disconnected *");
	OLED_Print(0, 4, displaystr, 1);
	setiotf(0);

}

void ICACHE_FLASH_ATTR mqttDataCb(uint32_t *args, const char* topic, uint32_t topic_len, const char *data, uint32_t length)
{

	os_printf("MQTT: Data received\r\n");
	//os_printf("Upgrade flag:%d\n",system_upgrade_flag_check());
	if (system_upgrade_flag_check() != 0)
		{
		return;
		}
	if(data == NULL)
		{
		return;
		}


	char strTopic[topic_len + 1];
	os_memcpy(strTopic, topic, topic_len);
	strTopic[topic_len] = '\0';

	// the maximium length for data should be 21 (not true for iotf!)
	//if (length > 21)
	//{
	//	length=21;
	//}

	char strData[length + 1];
	os_memcpy(strData, data, length);
	strData[length] = '\0';
	//char tempdata[length+1];
	//os_memcpy(tempdata, data, length);
	//tempdata[length] = '\0';

	if(sysCfg.iotfreg_enable==1) {
		os_printf("MQTT: iotf registered mode data received\r\n");
		os_printf("Topic: %s\r\n",strTopic);
		os_printf("Data: %s\r\n",strData);
		if (strstr (strData, "led")) {
			os_printf("LED info received!\r\n");

			os_memcpy(strTopic, "/led/1", 6);
			strTopic[5] = strData[10];
			strTopic[6] = '\0';
			topic_len = 6;

			int c=0;

				while (13 + c + 2 < length) {
					strData[c] = data[13+c];
				      c++;
				   }
				   strData[c] = '\0';

			//os_printf("data: %s \r\n", strData);

		}
		if (strstr (strData, "display")) {
			os_printf("Display info received!\r\n");
			os_memcpy(strTopic, "/display/1", 10);
			strTopic[9] = strData[14];
			strTopic[10] = '\0';
			topic_len = 10;

			int c=0;

				while (18 + c + 3 < length) {
					strData[c] = data[18+c];
				      c++;
				   }
				   strData[c] = '\0';

			//os_printf("data: %s \r\n", strData);


		}

	}


//	os_printf("MQTT topic: %s len:%d, data: %s \r\n", strTopic, topic_len, strData);
	//os_printf("strTopic: %s StrData: %s \r\n",strTopic, strData);
	// this is for the LED mqtt messages
	char ledNum=strTopic[topic_len-1];

	os_printf("strTopic: %s StrData: %s lednum: %d\r\n",strTopic, strData, ledNum-'0');
	if (strstr (strTopic, "/led/")) {
		os_printf("LED %d is now: %s \r\n", ledNum-'0', strData);

		
		if(ledNum=='1') {
			currGPIO12State=atoi(strData);
			ioGPIO(currGPIO12State,12);
			//pwm_set_duty(currGPIO12State, 1);
			//pwm_set_duty(currGPIO12State,1);
			//pwm_start();
		}

		if(ledNum=='2') {
			currGPIO13State=atoi(strData);
			//pwm_set_duty(currGPIO13State,2);
			//pwm_set_duty(currGPIO13State, 2);
			//pwm_start();
			ioGPIO(currGPIO13State,13);
		}

		if(ledNum=='3') {
			currGPIO15State=atoi(strData);
			//pwm_set_duty(currGPIO15State,0);
			//pwm_set_duty(currGPIO15State, 0);
			//pwm_start();
			ioGPIO(currGPIO15State,15);
		}
	}


	if (strstr (strTopic, "/display/")) {
//		os_printf("OLED %d msg: %s \r\n",ledNum-'0', strData);

		os_strcpy(oledstr, "                      ");
		//oledstr="xxxxxxxxxxxxxxxxxxxxxxx";
		strncpy(oledstr,(unsigned char *)(strData), strlen(strData));
		oledstr[21] = '\0';
//		os_printf("OLED %d msg: %s len: %d\r\n",ledNum-'0', strData, length);
		if(ledNum=='0') {
//			os_printf("OLED 0 message \r\n");
			//OLED_Print(0, 2, "                     ", 1);
			OLED_Print(0, 2, (unsigned char *)oledstr, 1);

		}
		if(ledNum=='1') {
//			os_printf("OLED 1 message \r\n");
			//OLED_Print(0, 3, "                     ", 1);
			OLED_Print(0, 3, (unsigned char *)oledstr, 1);

		}

		if(ledNum=='2') {
//			os_printf("OLED 2 message \r\n");
			//OLED_Print(0, 4, "                     ", 1);
			OLED_Print(0, 4, (unsigned char *)oledstr, 1);
			}

		if(ledNum=='3') {
//			os_printf("OLED 3 message \r\n");
			//OLED_Print(0, 5, "                     ", 1);
			OLED_Print(0, 5, (unsigned char *)oledstr, 1);
			}
		if(ledNum=='4') {
//			os_printf("OLED 4 message \r\n");
			//OLED_Print(0, 6, "                     ", 1);
			OLED_Print(0, 6, (unsigned char *)oledstr, 1);
			}
		}
	os_printf("Free heap size:%d\n",system_get_free_heap_size());
}

void ICACHE_FLASH_ATTR mqttPublishedCb(uint32_t *args)
{
//    MQTT_Client* client = (MQTT_Client*)args;
//    os_printf("MQTT: Published\r\n");
}


LOCAL void ICACHE_FLASH_ATTR startmqtt_cb(void *arg)
{
	os_timer_disarm(&startmqtt);

	//if(sysCfg.mqtt_enable==1) {
		MQTT_Connect(&mqttClient);
	//} else {
	//	MQTT_Disconnect(&mqttClient);
	//}

}

/*****************************************************************************
 * FunctionName : user_set_softap_config
 * Description  : set SSID and password of ESP8266 softAP
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR
user_set_softap_config(void)
{
   struct softap_config config;

   wifi_softap_get_config(&config); // Get config first. If you dont do this you need to rebuild whole struct

   os_memset(config.ssid, 0, 32);
   os_memcpy(config.ssid, (unsigned char *)sysCfg.mqtt_devid, 14);
   config.ssid_len = 0;// or its actual length
//   config.ssid_hidden = 1;
   wifi_softap_set_config(&config);// Set ESP8266 softap config .
}


void ICACHE_FLASH_ATTR http_callback_IP(char * response, int http_status, char * full_response)
{
os_printf("http_status=%d\n", http_status);
if (http_status != HTTP_STATUS_GENERIC_ERROR) {
	os_printf("strlen(full_response)=%d\n", strlen(full_response));
	os_printf("response=%s<EOF>\n", response);
	os_printf("External IP address=%s\n", response);
	// got an external IP - lets try IoTF
	if(sysCfg.iotfquick_enable==1||sysCfg.iotfreg_enable==1) {
		unsigned char displaystr[32];
		os_sprintf((char *)displaystr, "Connecting to IoTF   ");
		OLED_Print(0, 4, " Connecting to IoTF  ", 1);

		char temp[64];
		char username[20]="";
		char password[20]="";

		if(sysCfg.iotfreg_enable==1) {

			os_sprintf(temp,"%s.messaging.internetofthings.ibmcloud.com",sysCfg.iotf_org);
			os_sprintf(username,"use-token-auth");
			os_sprintf(password,"%s",sysCfg.iotf_token);
			os_printf("url=%s\n", temp);
			os_printf("username=%s\n", username);
			os_printf("pass=%s\n", password);
			MQTT_InitConnection(&mqttClient, temp, 1883, 0 );
			os_sprintf(temp,"d:%s:iotbox:%s",sysCfg.iotf_org,sysCfg.mqtt_devid);
			os_printf("connect=%s\n", temp);
			MQTT_InitClient(&mqttClient, (uint8_t *)temp, username, password, sysCfg.mqtt_keepalive,1);

		} else {

			os_sprintf(temp,"quickstart.messaging.internetofthings.ibmcloud.com");
			MQTT_InitConnection(&mqttClient, temp, 1883, 0 );
			os_sprintf(temp,"d:quickstart:iotbox:%s",sysCfg.mqtt_devid);
			MQTT_InitClient(&mqttClient, (uint8_t *)temp, username, password, sysCfg.mqtt_keepalive,1);

		}

		MQTT_OnConnected(&mqttClient, mqttConnectedCb);
		//MQTT_InitLWT(&mqttClient, (uint8_t *)sysCfg.mqtt_lwt, "offline", 0, 1);
		MQTT_OnDisconnected(&mqttClient, mqttDisconnectedCb);
		MQTT_OnPublished(&mqttClient, mqttPublishedCb);
		MQTT_OnData(&mqttClient, mqttDataCb);
		MQTT_Connect(&mqttClient);
		setiotf(1);

		os_sprintf(temp,"value:%d", getiotf());
		os_printf( temp);

		}
	} else {
		if(sysCfg.iotfquick_enable==1||sysCfg.iotfreg_enable==1) {
			OLED_Print(0, 4, "No External route  ", 1);
			}
		setiotf(0);
	}
}
void ICACHE_FLASH_ATTR wifiConnectCb(uint8_t status)
{
	if(status == STATION_GOT_IP){
		os_printf("Trying to find external IP address\n");
		http_get("http://wtfismyip.com/text", http_callback_IP);
	}
	// moved outside of check to allow mqtt in standalone mode

}

//Main routine
void ICACHE_FLASH_ATTR user_init(void) {
	bool aa;
	aa= system_update_cpu_freq(160);


	stdoutInit();	
	//os_delay_us(100000);


	CFG_Load();

    // ESP8266 softAP set config.
    user_set_softap_config();

	ioInit();
	
	OLEDInit();

	WIFI_Connect(wifiConnectCb);
	
	httpdInit(builtInUrls, sysCfg.httpd_port);

	if(sysCfg.sensor_dht22_enable) 
		DHTInit(SENSOR_DHT22, 5000);
		
	if(sysCfg.sensor_ds18b20_enable) 
		ds_init(5000);

	broadcastd_init();
	os_printf("\r\nSDK version: %s\n", system_get_sdk_version());
	os_printf("\nIoTBox Ready\n");	
	os_printf("Free heap size:%d\n",system_get_free_heap_size());
	os_printf("ESP8266 speed:%d\n",system_get_cpu_freq());

	
#ifdef CGIPWM_H	
	//Mind the PWM pin!! defined in pwm.h
	//LOCAL uint32 duty = 500;
	//LOCAL uint32 io_info[][3] = {{PERIPHS_IO_MUX_MTDO_U, FUNC_GPIO15, 15},{PERIPHS_IO_MUX_MTDI_U, FUNC_GPIO12, 12},{PERIPHS_IO_MUX_MTCK_U, FUNC_GPIO13, 13}};

	//pwm_init(1000, &duty, 3, io_info);
	//pwm_start();


#endif


	if(sysCfg.mqtt_enable==1) {
		os_timer_disarm(&startmqtt);
		os_timer_setfn(&startmqtt, (os_timer_func_t *)startmqtt_cb, (void *)0);
		os_timer_arm(&startmqtt, 10000, 1);
		unsigned char displaystr[32];
		os_sprintf((char *)displaystr, "Mode: MQTT   ");
		OLED_Print(0, 4, displaystr, 1);
		MQTT_InitConnection(&mqttClient, (uint8_t *)sysCfg.mqtt_host, sysCfg.mqtt_port, sysCfg.mqtt_use_ssl );
		MQTT_InitClient(&mqttClient, (uint8_t *)sysCfg.mqtt_devid, (uint8_t *)sysCfg.mqtt_user, (uint8_t *)sysCfg.mqtt_pass, sysCfg.mqtt_keepalive,1);
		MQTT_OnConnected(&mqttClient, mqttConnectedCb);
		MQTT_InitLWT(&mqttClient, (uint8_t *)sysCfg.mqtt_lwt, "offline", 0, 1);
		MQTT_OnDisconnected(&mqttClient, mqttDisconnectedCb);
		MQTT_OnPublished(&mqttClient, mqttPublishedCb);
		MQTT_OnData(&mqttClient, mqttDataCb);
	}
	if(sysCfg.iotfquick_enable==1) {
		unsigned char displaystr[32];
		os_sprintf((char *)displaystr, "Mode: IoTF Quickstart");
		OLED_Print(0, 4, displaystr, 1);
	}

	if(sysCfg.iotfreg_enable==1) {
		unsigned char displaystr[32];
		os_sprintf((char *)displaystr, "Mode: IoTF Registered");
		OLED_Print(0, 4, displaystr, 1);
	}


}


