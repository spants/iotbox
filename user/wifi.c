/*
 * wifi.c
 *
 *  Created on: Dec 30, 2014
 *      Author: Minh
 */
#include "wifi.h"
#include "user_interface.h"
#include "osapi.h"
#include "espconn.h"
#include "os_type.h"
#include "mem.h"
#include "debug.h"
#include "config.h"

#include "driver/i2c.h"
#include "driver/i2c_oled.h"
// these are declared in user_interface.h, so why is the compiler complaining
extern uint8 system_upgrade_flag_check();

#include "global.h"
int retry;
char oledstr[32];

static ETSTimer WiFiLinker;
WifiCallback wifiCb = NULL;
static uint8_t wifiStatus = STATION_IDLE, lastWifiStatus = STATION_IDLE;

static void ICACHE_FLASH_ATTR wifi_check_ip(void *arg)
{
	//INFO("Check IP routine\r\n");
	if (system_upgrade_flag_check() != 0)
		{
		return;
		}
	struct ip_info ipConfig;

	os_timer_disarm(&WiFiLinker);
	wifi_get_ip_info(STATION_IF, &ipConfig);
	wifiStatus = wifi_station_get_connect_status();
	//wifi_station_set_auto_connect(TRUE);

	if (wifiStatus == STATION_GOT_IP && ipConfig.ip.addr != 0)
	{
		os_timer_setfn(&WiFiLinker, (os_timer_func_t *)wifi_check_ip, NULL);
		os_timer_arm(&WiFiLinker, 1000, 0);
		static uint8_t wifiStatus = STATION_IDLE;
		//wifi_station_set_auto_connect(FALSE);
		//INFO("Got IP\r\n");
		// added the next paragraph
		struct ip_info ipConfig;
		unsigned char displaystr[32];

		struct ip_info pTempIp;
		wifi_get_ip_info(0x00, &pTempIp);
		os_sprintf((char *)displaystr, " IP: %d.%d.%d.%d",IP2STR(&pTempIp.ip));

		os_strcpy(oledstr, "                      ");

		strncpy(oledstr,(unsigned char *)(displaystr), strlen(displaystr));
		oledstr[21] = '\0';

		OLED_Print(0, 1, (unsigned char *)oledstr, 1);

		if (getnoip() == 1)
		{
			system_restart();
		}


	}
	else
	{

		if(wifi_station_get_connect_status() == STATION_WRONG_PASSWORD)
		{
			INFO("STATION_WRONG_PASSWORD\r\n");
			OLED_Print(0, 1, " * Wrong Password *  ", 1);
			wifi_set_opmode(SOFTAP_MODE);

		}
		else if(wifi_station_get_connect_status() == STATION_NO_AP_FOUND)
		{
			OLED_Print(0, 1, " * No Access Point *  ", 1);
			INFO("STATION_NO_AP_FOUND\r\n");

			wifi_station_disconnect();  //disable auto-reconnect

			// setup timer to recheck connectivity and then reboot if it finds an ip
			INFO("***NO AP\r\n");
			setnoip(1);
			retry = false;
			// fix for scan is here
			// https://github.com/jeelabs/esp-link/issues/29


		}
		else if(wifi_station_get_connect_status() == STATION_CONNECT_FAIL)
		{
			OLED_Print(0, 1, " * Connect Failed *  ", 1);
			INFO("Connect failed\r\n");
			wifi_set_opmode(SOFTAP_MODE);

		}
		else
		{
//			OLED_Print(0, 7, "                     ", 1);
		}
		os_timer_setfn(&WiFiLinker, (os_timer_func_t *)wifi_check_ip, NULL);

		//INFO("set timer now\r\n");

		if (getnoip() == 0)
		{
			os_timer_arm(&WiFiLinker, 1000, 0);
		} else {
			if (retry) {
				wifi_station_connect();//reconnect
				retry = false; //unset the retry flag
				os_timer_arm(&WiFiLinker, 1000, 0);
			} else {
				os_timer_arm(&WiFiLinker, 120000, 0);
				retry = true;
			}
		}

	}
	if(wifiStatus != lastWifiStatus){
//		OLED_Print(0, 7, "                     ", 1);
		lastWifiStatus = wifiStatus;
		if(wifiCb)
			wifiCb(wifiStatus);
		os_printf("\nchanged status\n");

	}
	else
	{
//		INFO("STATION_IDLE\r\n");
	}

}

void ICACHE_FLASH_ATTR WIFI_Connect(WifiCallback cb)
{
	struct station_config stationConf;
	struct ip_info info;

	INFO("WIFI_INIT\r\n");
	
	os_timer_disarm(&WiFiLinker);
	
	//set to always be sta-ap
	wifi_set_opmode(STATIONAP_MODE);


	wifi_station_set_auto_connect(FALSE);
	wifiCb = cb;

	os_memset(&stationConf, 0, sizeof(struct station_config));

	os_printf("attach to %s %S\n", sysCfg.sta_ssid,sysCfg.sta_pass);

	os_sprintf((char *)stationConf.ssid, "%s", sysCfg.sta_ssid);
	os_sprintf((char *)stationConf.password, "%s", sysCfg.sta_pass);


	wifi_get_ip_info(STATION_IF, &info);
	char *dhcp = (char *)sysCfg.sta_mode; 
	//os_printf("**mode: %s\n", (char *)sysCfg.sta_mode);
	char *ip, *mask, *gw;
	if (!dhcp || strcmp(dhcp, "dhcp") != 0)
	{
		ip = (char *)sysCfg.sta_ip; 
		mask = (char *)sysCfg.sta_mask;
		gw = (char *)sysCfg.sta_gw;
		if (ip)
			info.ip.addr = ipaddr_addr(ip);
		if (mask)
			info.netmask.addr = ipaddr_addr(mask);
		if (gw)
			info.gw.addr = ipaddr_addr(gw);
		
		wifi_set_ip_info(STATION_IF, &info);
	}

	wifi_get_ip_info(SOFTAP_IF, &info);
	ip = (char *)sysCfg.ap_ip; 
	mask = (char *)sysCfg.ap_mask;
	gw = (char *)sysCfg.ap_gw;
	if (ip)
		info.ip.addr = ipaddr_addr(ip);
	if (mask)
		info.netmask.addr = ipaddr_addr(mask);
	if (gw)
		info.gw.addr = ipaddr_addr(gw);
	
	if (wifi_get_opmode() != STATION_MODE)
		wifi_set_ip_info(SOFTAP_IF, &info);

	wifi_station_set_config(&stationConf);
		
	os_timer_disarm(&WiFiLinker);
	os_timer_setfn(&WiFiLinker, (os_timer_func_t *)wifi_check_ip, NULL);
	os_timer_arm(&WiFiLinker, 1000, 0);

	wifi_station_set_auto_connect(TRUE);
	wifi_station_connect();
	
}

