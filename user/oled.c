#include "ets_sys.h"
#include "osapi.h"
#include "espmissingincludes.h"

#include "wifi.h"
#include "user_interface.h"
#include "espconn.h"

#include "sntp.h"
#include "time_utils.h"
#include "driver/i2c.h"
#include "driver/i2c_oled.h"
#include "dht22.h"
#include "ds18b20.h"
#include "config.h"
// these are declared in user_interface.h, so why is the compiler complaining
extern uint8 system_upgrade_flag_check();

static volatile bool OLED;
static ETSTimer oledTimer;

static void ICACHE_FLASH_ATTR pollOLEDCb(void * arg){
	os_timer_disarm(&oledTimer);
	if (system_upgrade_flag_check() != 0)
		{
		return;
		}
	//OLED_CLS();
	char displaystr[32];
	char temp[16];
	char oledstr[32];

		//unsigned long epoch = sntp_time+(sntp_tz*3600);
		//epoch=epoch%86400;
		//unsigned int hour=epoch/3600;

	if(sysCfg.sensor_dht22_enable) {
		dht_temp_str(temp);

		os_sprintf(displaystr, "T: %sc  ", temp);
		OLED_Print(0, 7, (unsigned char *)displaystr, 1);
		dht_humi_str(temp);
		os_sprintf(displaystr, "H: %s%% ", temp);
		OLED_Print(11, 7, (unsigned char *)displaystr, 1);
		}
		
	if(sysCfg.sensor_ds18b20_enable) {
		ds_str(temp,0);

		os_sprintf(displaystr, " Temperature: %sc", temp);

		os_strcpy(oledstr, "                      ");

		strncpy(oledstr,(unsigned char *)(displaystr), strlen(displaystr));
		oledstr[21] = '\0';

		OLED_Print(0, 7, (unsigned char *)oledstr, 1);
		}
	 os_timer_arm(&oledTimer, 5000, 1);
}


void ICACHE_FLASH_ATTR OLEDInit(void) {

  i2c_init();
  OLED = OLED_Init();

  OLED_Print(0, 0, (unsigned char *)sysCfg.mqtt_devid, 1);


  OLED_Print(16, 0, (unsigned char *)FWVER, 1);
  OLED_Print(0, 7, (unsigned char *)"Tony.Neal@uk.ibm.com", 1);


 if (sysCfg.mqtt_enable==0&&sysCfg.iotfquick_enable==0&&sysCfg.iotfreg_enable==0) {
	 os_timer_setfn(&oledTimer, pollOLEDCb, NULL);
	 os_timer_arm(&oledTimer, 5000, 1);
 	 }

}
