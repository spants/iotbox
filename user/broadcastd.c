#include "espmissingincludes.h"
#include "c_types.h"
#include "user_interface.h"
#include "espconn.h"
#include "mem.h"
#include "osapi.h"
#include "io.h"
#include "broadcastd.h"
#include "dht22.h"
#include "ds18b20.h"
#include "config.h"
#include "mqtt.h"
#include "utils.h"
#include "httpclient.h"
#include "global.h"
// added next few lines
#include "driver/i2c.h"
#include "driver/i2c_oled.h"
// these are declared in user_interface.h, so why is the compiler complaining

int shown;
extern uint8 system_upgrade_flag_check();

MQTT_Client mqttClient;

/*
 * ----------------------------------------------------------------------------
 * "THE MODIFIED BEER-WARE LICENSE" (Revision 42):
 * Mathew Hall wrote this file. As long as you
 * retain
 * this notice you can do whatever you want with this stuff. If we meet some
 * day,
 * and you think this stuff is worth it, you can buy sprite_tm a beer in return.
 * ----------------------------------------------------------------------------
 */

static ETSTimer MQTTbroadcastTimer;
static ETSTimer broadcastTimer; 
 
static void ICACHE_FLASH_ATTR broadcastReading(void *arg) {
	//os_printf("Upgrade flag:%d\n",system_upgrade_flag_check());
	if (system_upgrade_flag_check() != 0)
		{
		return;
		}

    char buf[384];
	char buf2[255];
	char buf3[255];
	char t1[32];
	char t2[32];
	char t3[32];
	
	//double expand as sysCfg.broadcastd_url contains placeholders as well
//	os_sprintf(buf2,"http://%s:%d/%s",sysCfg.broadcastd_host,(int)sysCfg.broadcastd_port,sysCfg.broadcastd_url);
	os_sprintf(buf2,"%s",sysCfg.broadcastd_host);
	os_sprintf(buf3,"%s",sysCfg.broadcastd_url);
	
	if(sysCfg.sensor_dht22_enable)  {
		dht_temp_str(t2);
		dht_humi_str(t3);
// mod by tony - just want temperature to be sent
//		os_sprintf(buf,buf2,currGPIO12State,currGPIO13State,currGPIO15State,"N/A",t2,t3);
//		os_sprintf(buf,buf2,t2);
		os_sprintf(buf,buf3,t2);

	}
	
	if(sysCfg.sensor_ds18b20_enable)  { // If DS18b20 daemon is enabled, then send up to 3 sensor's data instead
		ds_str(t1,0);

// mod by tony - just want temperature to be sent
//		if(numds>1) ds_str(t2,1); //reuse to save space
//		if(numds>2)  ds_str(t3,2); //reuse to save space
//		os_sprintf(buf,buf2,currGPIO12State,currGPIO13State,currGPIO15State,t1,t2,t3);

//		os_sprintf(buf,buf2,t1);
		os_sprintf(buf,buf3,t1);
	}


//	http_get(buf, http_callback_example);
	http_post(buf2,buf, http_callback_example);


//	os_printf("Sent HTTP GET: %s\n\r",buf);
	os_printf("Sent HTTP post URL: %s\n\r",buf2);
	os_printf("Sent HTTP post DATA: %s\n\r",buf);
}
 

static ICACHE_FLASH_ATTR void MQTTbroadcastReading(void* arg){

	if (system_upgrade_flag_check() != 0)
		{
		return;
		}
	if(sysCfg.iotfquick_enable==1||sysCfg.iotfreg_enable==1) {

		if (getiotf() == 0||getnoip() == 1)
			{
			//unsigned char displaystr[32];
			//os_sprintf((char *)displaystr,"  ** IoTF Error **   ");
			//OLED_Print(0, 4, displaystr, 1);
			return;
			}


		os_printf("Sending MQTT on IoTF\n");

		if (shown == 0) {
			unsigned char displaystr[32];
			os_sprintf((char *)displaystr, "   IoTF Connected     ");
			OLED_Print(0, 4, displaystr, 1);
			shown = 1;
		}
	}


	os_timer_disarm(&MQTTbroadcastTimer);

	if(sysCfg.sensor_dht22_enable) {
		struct sensor_reading* result = readDHT();
		if(result->success) {
			char temp[32];
			char humid[32];
			//char temp2[64];
			char topic[64];
			int len;
			char displaystr[32];
			char oledstr[32];

			dht_temp_str(temp);
			dht_humi_str(humid);

			if(sysCfg.mqtt_enable==1) {

				len = os_strlen(temp);
				os_sprintf(topic,"%s",sysCfg.mqtt_ds18b20_temp_pub_topic);
				MQTT_Publish(&mqttClient,topic,temp,len,0,0);
				os_printf("Published \"%s\" to topic \"%s\"\n",temp,topic);

				len = os_strlen(humid);
				os_sprintf(topic,"%s",sysCfg.mqtt_dht22_humi_pub_topic);
				MQTT_Publish(&mqttClient,topic,humid,len,0,0);
				os_printf("Published \"%s\" to topic \"%s\"\n",humid,topic);

			} else {

				os_sprintf(topic,"{\"d\":{\"Temperature\":%s,\"Humidity\":%s}}",temp,humid);
				len = os_strlen(topic);
				MQTT_Publish(&mqttClient,"iot-2/evt/status/fmt/json",topic,len,0,0);
				os_printf("Published %s to IoTF\n",topic);
			}

			os_sprintf(displaystr, "T: %sc  ", temp);
			OLED_Print(0, 7, (unsigned char *)displaystr, 1);
			os_sprintf(displaystr, "H: %s%% ", humid);
			OLED_Print(11, 7, (unsigned char *)displaystr, 1);

			}
		}

	if(sysCfg.sensor_ds18b20_enable) {
			struct sensor_reading* result = read_ds18b20();
			if(result->success) {
				char temp[32];
				char topic[128];
				int len;
				char displaystr[32];
				char oledstr[32];

				ds_str(temp,0);

				if(sysCfg.mqtt_enable==1) {
					len = os_strlen(temp);
					os_sprintf(topic,"%s",sysCfg.mqtt_ds18b20_temp_pub_topic);
					MQTT_Publish(&mqttClient,topic,temp,len,0,0);
					os_printf("Published \"%s\" to topic \"%s\"\n",temp,topic);

				}else{
					os_sprintf(topic,"{\"d\":{\"Temperature\":%s}}",temp);
					len = os_strlen(topic);
					MQTT_Publish(&mqttClient,"iot-2/evt/status/fmt/json",topic,len,0,0);
					os_printf("Published %s to IoTF\n",topic);
				}

				os_sprintf(displaystr, " Temperature: %sc", temp);
				os_strcpy(oledstr, "                      ");
				strncpy(oledstr,(unsigned char *)(displaystr), strlen(displaystr));
				oledstr[21] = '\0';
				OLED_Print(0, 7, (unsigned char *)oledstr, 1);
			}
		}
	os_timer_arm(&MQTTbroadcastTimer, 5000, 1);
}



void ICACHE_FLASH_ATTR broadcastd_init(void){

	if(sysCfg.mqtt_enable==1||sysCfg.iotfquick_enable==1||sysCfg.iotfreg_enable==1) {
		os_printf("Arming MQTT broadcast timer\n");
		os_timer_setfn(&MQTTbroadcastTimer, MQTTbroadcastReading, NULL);
		//os_timer_arm(&MQTTbroadcastTimer, 10000, 1);
		os_timer_arm(&MQTTbroadcastTimer, 5000, 1);
	}
	
	if(sysCfg.broadcastd_enable==1) {
		os_printf("Arming HTTP broadcast timer\n");  	
		os_timer_setfn(&broadcastTimer, broadcastReading, NULL);
		os_timer_arm(&broadcastTimer, 60000, 1);		
	}
}


