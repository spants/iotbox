/*
 * wifi.h
 *
 *  Created on: Dec 30, 2014
 *      Author: Minh
 */

#ifndef IOTBOX_V0_2_USER_WIFI_H_
#define IOTBOX_V0_2_USER_WIFI_H_
#include "os_type.h"
typedef void (*WifiCallback)(uint8_t);
void WIFI_Connect(WifiCallback cb);


#endif /* IOTBOX_V0_2_USER_WIFI_H_ */
