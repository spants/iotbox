/*
 * global.h
 *
 *  Created on: 21 Oct 2015
 *      Author: spants
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_

int getiotf();
void setiotf(int v);

int getnoip();
void setnoip(int v);

#endif /* INCLUDE_GLOBAL_H_ */
